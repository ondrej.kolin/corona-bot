from bs4 import BeautifulSoup as Soup
from time import sleep
import requests
URL="https://www.idnes.cz/"
SLEEP_TIME = 300
STATE_FILE = "/run/corona_state"
DISCORD_WEBHOOK = None

def load_state():
    try:
        with open(STATE_FILE) as f:
            state = f.readline()
            try:
                return int(state)
            except:
                print("Last state doesn't seem to be numeric!")
                return 0 
        return 0
    except Exception as e: 
        print(f"Error: {e}. State could not have been read")
    return 0

def save_state(state):
    try:
        with open(STATE_FILE, "wt") as f:
            f.write(f"{state}\n")
    except Exception as e:
        print(f"Error: {e}! State has not been saved.")

def actual_state():
    req = requests.get(URL)
    print(f"It took just {req.elapsed.microseconds/1000000} to download {URL} ")
    soup = Soup(req.content, features="lxml")
    data = soup.find("a", attrs = {"class": "megapruh-logo"}).find("b").text
    try:
        return int(data)
    except:
        print(f"Could not read the int value from the {URL} {data}.")
        return None

def notify_world(state):
    if DISCORD_WEBHOOK is None:
        print("Error: DISCORD_WEBHOOK is unset! Without it I can not post updates to Discord")
    try:
        req = requests.post(DISCORD_WEBHOOK, data={'username':'Doomsday', "content": f"Konec se blíží! Nakažených coronavirem je již {state}"})
        print("Notifying the population took just {req.elapsed.microseconds/1000000}")
    except Exception as e:
        print(f"Error: {e}! The humankind wasn't informed at all!")



def main():
    if DISCORD_WEBHOOK is None:
        print("Error: DISCORD_WEBHOOK is unset! Without it I can not post updates to Discord")
    print("Bot started")
    state = load_state()
    while(True):
        new_state = actual_state()
        if new_state > state:
            print(f"We have a new maximum - {new_state}!")
            state = new_state
            save_state(state)
            notify_world(state)
        else:
            print(f"Number has not changed {state}")
        print(f"Going to sleep for {SLEEP_TIME}")
        sleep(SLEEP_TIME)


if __name__ == "__main__":
    main()
